exports.replaceWithEvaluate = function (content, data) {
  return content.replace(/{{(.*?)}}/, function (match, p1) {
    let value = eval(p1);
    if(value === undefined || value === null) {
      value = "";
    }
    return String(value);
  });
}
