var fs = require('fs');
var _get = require('lodash/get');
var moment = require('moment');
module.exports = function(app) {
  app.config([
    'formioComponentsProvider',
    function(formioComponentsProvider) {
      formioComponentsProvider.register('datetime', {
        title: 'Date / Time',
        template: 'formio/components/datetime.html',
        tableView: function(data, options) {
          data = _.isDate(data) ? data.toISOString() : data;
          return options.$interpolate('<span>{{ "' + data + '" | date: "' + options.component.format + '" }}</span>')();
        },
        group: 'advanced',
        controller: ['$scope', '$timeout', 'FormioUtils', 'dateFilter', 'uibDateParser', function($scope, $timeout, FormioUtils, dateFilter, uibDateParser) {
        /* eslint-enable no-unused-vars */
          // Close calendar pop up when tabbing off button
          $scope.allowInput = true;
          if($scope.component.hasOwnProperty("allowInput")) {
            $scope.allowInput = $scope.component.allowInput;
          }
          else if($scope.component.widget && $scope.component.widget.hasOwnProperty("allowInput")) {
            $scope.allowInput = $scope.component.widget.allowInput;
          }
          $scope.onKeyDown = function(event) {
            return event.keyCode === 9 ? false : $scope.calendarOpen;
          };
          $scope.inputFocused = false;
          $scope.onBlur = function(event) {
            $scope.inputFocused = false;
            if (!$scope._data.value) {
              $scope.data[$scope.component.key] = null;
              event.target.value = '';
            }
          };
          $scope.onInputFocus = function () {
            $scope.inputFocused = true;
            $scope.calendarOpen = $scope.autoOpen;
          }
          var legacyParseDate = function (value, format) {
            var parsedDate = uibDateParser.parse(value, format);
            return dateFilter(parsedDate, format) === value ? parsedDate : null;
          }
          var dateValue = function() {
            // If the date is set, then return the true date value.
            if ($scope.data[$scope.component.key]) {
              if($scope.data[$scope.component.key] instanceof Date) {
                return $scope.data[$scope.component.key];
              }
              var parsedDt;
              if ($scope.component.format) {
                parsedDt = legacyParseDate($scope.data[$scope.component.key], $scope.component.format);
                if(parsedDt instanceof Date) {
                  return parsedDt;
                }
              }
              if(!$scope.component.enableTime) {
                if($scope.data[$scope.component.key].trim().length >= 10) {
                  parsedDt = moment($scope.data[$scope.component.key].trim().substring(0, 10), "YYYY-MM-DD");
                  if(parsedDt.isValid()) {
                    return parsedDt.toDate();
                  }
                }
              }
              parsedDt = moment($scope.data[$scope.component.key], moment.ISO_8601, true);
              if(parsedDt.isValid()) {
                return parsedDt.toDate();
              }
            }
            var defaultValue = $scope.component.defaultDate ? $scope.component.defaultDate : $scope.component.defaultValue;
            if(defaultValue === null || defaultValue === undefined) {
              return null;
            }
            if($scope.component.format) {
              var defaultDt = legacyParseDate(defaultValue, $scope.component.format);
              if(defaultDt instanceof Date) {
                return defaultDt;
              }
            }
            if(!$scope.component.enableTime) {
              if(defaultValue.trim().length >= 10) {
                defaultDt = moment(defaultValue.trim().substring(0, 10), "YYYY-MM-DD");
                if(defaultDt.isValid()) {
                  return defaultDt.toDate();
                }
              }
            }
            defaultDt = moment(defaultValue, moment.ISO_8601, true);
            if(defaultDt.isValid()) {
              return defaultDt.toDate();
            }
            defaultDt = FormioUtils.getDateSetting(defaultValue);
            return defaultDt ? defaultDt : null;
          };
          $scope._data = {value: dateValue()}

          // Ensure the date value is always a date object when loaded, then unbind the watch.
          $scope.$watch('_data.value', function() {
            if($scope._data.value) {
              var newValue = $scope._data.value;
              if(!$scope.component.enableTime) {
                newValue = moment(newValue).format('YYYY-MM-DD');
              }
              if($scope.data[$scope.component.key] !== newValue) {
                $scope.data[$scope.component.key] = newValue;
              }
            }
            else if ((!$scope.inputFocused || !$scope.allowInput) && $scope.data[$scope.component.key] !== $scope._data.value) {
              $scope.data[$scope.component.key] = $scope._data.value;
            }
          });
          $scope.$watch('data.' + $scope.component.key, function() {
            $scope._data.value = dateValue();
          });

          // Watch for changes to the meridian settings to synchronize the submissionGrid and component view.
          $scope.$watch('component.timePicker.showMeridian', function(update) {
            // Remove any meridian reference, because were not in 12 hr.
            if (!$scope.component.enableTime || !update) {
              $scope.component.format = $scope.component.format.replace(/ a/, '');
              return;
            }

            // If we're missing the meridian string and were in 12 hr, add it.
            if (update && $scope.component.format.indexOf(' a') === -1) {
              $scope.component.format += ' a';
            }
          });
          $scope.component.datePicker.minDate = FormioUtils.getDateSetting(_get($scope.component, 'datePicker.minDate'));
          $scope.component.datePicker.maxDate = FormioUtils.getDateSetting(_get($scope.component, 'datePicker.maxDate'));

          $scope.autoOpen = true;
          $scope.onClosed = function() {
            $scope.autoOpen = false;
            $timeout(function() {
              $scope.autoOpen = true;
            }, 250);
          };
        }],
        settings: {
          autofocus: false,
          input: true,
          tableView: true,
          label: 'Date Time',
          key: 'datetime',
          placeholder: '',
          format: 'yyyy-MM-dd HH:mm a',
          enableDate: true,
          enableTime: true,
          defaultDate: '',
          datepickerMode: 'day',
          datePicker: {
            showWeeks: true,
            startingDay: 0,
            initDate: '',
            minMode: 'day',
            maxMode: 'year',
            yearRows: 4,
            yearColumns: 5,
            minDate: null,
            maxDate: null
          },
          timePicker: {
            hourStep: 1,
            minuteStep: 1,
            showMeridian: true,
            readonlyInput: false,
            mousewheel: true,
            arrowkeys: true
          },
          protected: false,
          persistent: true,
          hidden: false,
          clearOnHide: true,
          validate: {
            required: false,
            custom: ''
          }
        }
      });
    }
  ]);
  app.run([
    '$templateCache',
    'FormioUtils',
    function($templateCache, FormioUtils) {
      $templateCache.put('formio/components/datetime.html', FormioUtils.fieldWrap(
        fs.readFileSync(__dirname + '/../templates/components/datetime.html', 'utf8')
      ));
    }
  ]);
};
